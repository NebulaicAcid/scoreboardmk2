import { Component, OnInit } from '@angular/core';
import { ViewHandler } from '../viewhandler.service';

//Service Objects
var view_handler = new ViewHandler();

@Component({
  moduleId: module.id,
  selector: 'app-scores',
  templateUrl: 'scores.component.html',
  styleUrls: ['scores.component.css'],
})
export class ScoresComponent {
  score_state = view_handler.score_state;
}
