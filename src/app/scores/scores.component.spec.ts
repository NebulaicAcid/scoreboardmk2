/* tslint:disable:no-unused-variable */

import { By }           from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import {
  beforeEach, beforeEachProviders,
  describe, xdescribe,
  expect, it, xit,
  async, inject
} from '@angular/core/testing';

import { ScoresComponent } from './scores.component';

describe('Component: Scores', () => {
  it('should create an instance', () => {
    let component = new ScoresComponent();
    expect(component).toBeTruthy();
  });
});
