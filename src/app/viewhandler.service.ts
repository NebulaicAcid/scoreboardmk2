import { Injectable } from '@angular/core';

@Injectable()

export class ViewHandler {
   //Variables
   score_state: number;

   constructor(){ 
       this.score_state = 0;
   }

   //Methods
   toggleScores(): void {
       this.score_state ^= 0;
   }
}