import { Component } from '@angular/core';
import { NgSwitch, NgSwitchDefault, NgSwitchWhen } from '@angular/common';
import { ScoresComponent } from './scores/scores.component';
import { ViewHandler } from './viewhandler.service';

@Component({
  moduleId: module.id,
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.css'],
    directives: [NgSwitch, NgSwitchDefault, NgSwitchWhen, ScoresComponent]
})
export class AppComponent {
}
