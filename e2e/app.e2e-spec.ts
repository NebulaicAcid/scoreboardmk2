import { Scoreboardmk2Page } from './app.po';

describe('scoreboardmk2 App', function() {
  let page: Scoreboardmk2Page;

  beforeEach(() => {
    page = new Scoreboardmk2Page();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
